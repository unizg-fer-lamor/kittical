# About the repository #

This repository accompanies our paper _Recalibrating the KITTI Dataset Camera Setup for Improved Odometry Accuracy_ published at the European Conference on Mobile Robots 2021. It holds the KITTI calibration file containing all the calibration parameters necessary to recreate the results.

### Citation ###

To reference the paper please use the following citation:  

	
```
@inproceedings{Cvisic2021,
author = {Cvi{\v{s}}i{\'{c}}, Igor and Markovi{\'{c}}, Ivan and Petrovi{\'{c}}, Ivan},
booktitle = {European Conference on Mobile Robots (ECMR)},
title = {{Recalibrating the KITTI Dataset Camera Setup for Improved Odometry Accuracy}},
year = {2021}
}
```

### Evaluating odometry results ###

Custom calibration coordinate system (`CAM1`) is *NOT* aligned with the default calibration coordinate system (`KITTI`) due to different position of the principal point. Therefore, in order to evaluate the odometry results, trajectory computed in the custom coordinate system has to be transformed into the trajectory of the `KITTI` coordinate system as follows:

`KITTI = KITTI_TO_CAM1 * ODOM * CAM1_TO_KITTI`